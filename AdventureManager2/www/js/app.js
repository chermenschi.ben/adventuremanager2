$(function(){
    document.addEventListener("deviceready", onDeviceReady, false);
});
function onDeviceReady() {
    console.log('Device is ready');
    //setting devmode ready
    //note : Yes i am aware that this will reset devmode continously, Yes this is what i want
    var devmode = {
      enabled : false,
      times : 0
    };
    localStorage.setItem("devmode",JSON.stringify(devmode));
    //hiding devmode settings
   $(".experimental").hide();

   //initialising charactermanager
   Charactermanager.init();
   //allowing materialise CSS to change Select to make it work
   $('select').material_select();



}
//navigation
$('.button-collapse').sideNav();
//if navbutton is clicked
$('.side-nav a').click(function(){
    $('.spa').hide();
    if( $(this).data('show') == "CharacterEditor" && $('#currentId').val()==""){
      $('#IdEmpty').show();
    } else{
      $('#' + $(this).data('show')).show();
    }
    
    $('.button-collapse').sideNav('hide');
 });
//end of navigation


//personages
  //personage add
  $('#addPersonage').click( function(){
    console.log('traveling to the entrancehall');
    Charactermanager.addPersonage();
  });

  //personage edit
  $('ul').on('click','.editPersonage',function(){
    console.log('Lets change somebody their look');
    var id = $(this).data('personage');
    console.log('character id =  ' + id);
    Charactermanager.editPersonage(id);
  });

//personage save
$('#opslaanPersonage').click( function(){
  console.log('personage opslaan');
  var id = $('#currentId').val();
  Charactermanager.savePersonage(id);
});

  //personage delete
  $('ul').on('click','.deletePersonage',function(){
    console.log('deleting character commencing');
    var id= $(this).data('personage');
    console.log('character id = ' + id);
    Charactermanager.deletePersonage(id);
  });

  //Ping
  $('#ping').click(function(){
    $('.spa').hide();

    console.log('ping button pressed');
    var adress = $('#server').val();
    console.log('adress value appjs = ');
    console.log(adress);
    //giving the loader some text
    $('#loaderText').text("Pinging");
    $('#loaderDescription').text(adress);
    $("#loader").show();
    
    var serverdata = serverPing.ping(adress);
    console.log(' var server data : ');
    console.log(serverdata);
    console.log('-------------------');
    console.log(serverdata[0].response.result);
    console.log('-------------------');
    
});
//settings : 
  //kill local storage
  $('#KillLocalStorage').click(function(){
    if(confirm("are you sure, this cannot be undone and will reset your settings")){
    console.log("yes");
    localStorage.clear();
    Charactermanager.init();
    alert("If the characters don't seem deleted, please restart the app");
    }
    
  });
  
//devmode
  //download
  $('#download').click(function(){
    console.log("WARNING USER!!!");
    if(confirm("WARNING : You will destroy the current local storage data by doing this")){
      
        
        console.log("downloading data commencing");
        dataAccess.getCharacters();
        console.log("it survived this it appears");
        Charactermanager.init();
        console.log("data should be fixed now");
      }
  });

  //upload
  $('#upload').click(function(){
    console.log('readying upload');
    if(confirm("Have You deleted the Server Data?")){
      
        console.log("okay they wanna upload");
        var characters = Charactermanager.getLocalStorageJson();
        var catcher;
        var error;
        if(!characters){
          console.log('error no local data');
        }else{
          dataAccess.setCharacters(characters, catcher,error);

         
      }
    
  }
});

//delete server data
$('#DeleteServerData').click(function(){
  console.log('going to delete server data');
  if(confirm('This cannot be undone, do you with to proceed?')){
    console.log('commencing delete');
    dataAccess.deleteCharacters();
  }
});

  //activate
  $('#devmode').click(function(){
    var devmode_str = localStorage.getItem("devmode");
    if (devmode_str !== null){
       devmode = JSON.parse(devmode_str);
       console.log(devmode);
       if(devmode.enabled == false){
       if(devmode.times <= 6){
         console.log("devmode times");
         console.log(devmode.times);
         devmode.times = devmode.times +1;
         localStorage.setItem("devmode",JSON.stringify(devmode));
       } else{
         if(devmode.times == 7){
          devmode.enabled = true;
         localStorage.setItem("devmode",JSON.stringify(devmode));
         }
        
       }
     } 
      else{
      console.log("access granted");
      alert("Devmode active");
      alert("some functions might not work");
      $(".experimental").show();
     }
    }
  });
    //deactivate
    $("#devOff").click(function(){
      if(confirm("Are you sure?")){
        console.log("deactivating devmode");
        $(".experimental").hide();
      }
      
    });
    



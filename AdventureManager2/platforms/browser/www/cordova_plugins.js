cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-ping/www/ping.js",
        "id": "cordova-plugin-ping.ping",
        "pluginId": "cordova-plugin-ping",
        "clobbers": [
            "Ping"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-ping": "0.3.1",
    "cordova-plugin-whitelist": "1.3.2"
}
// BOTTOM OF METADATA
});
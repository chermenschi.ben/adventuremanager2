var dataAccess = function(){
  const target = 'http://81.243.185.166:3000/api/';
  var _personages = JSON.parse(localStorage.getItem("personages"));
  if (_personages == null){
    _personages = [];
  }


//grabbing all characters from database and ignoring local storage
  function getCharacters(callback,errorHandler){
    $.ajax({
      type: 'get',
      url : target + '/characters',
      success : function(data){
      //  console.log('showing data');
        //console.log(data);
        if(data[0] != null){

      //       console.log(data[0].personageLijst);
        var personages = JSON.parse(data[0].personageLijst);

       //console.log(personages[0].name);
       
        if (!personages[0].name){
          alert("Error Returned data doesn't contain a name for the character. Please Clear the database and try again. If this error reoccurs, Please contact the Developer!");
         
        } else{
           console.log('success');
           
           console.log('lets write that away');
           localStorage.setItem('personages', JSON.stringify(personages));
           Charactermanager.init();
           
        }
        }else{
          alert('no data on server');
        }
       
      },
      error : function(errorHandler){
        console.log("something went horribly wrong");
        console.log(errorHandler);
      }

    });
  };
  
  function setCharacters(personages,callback,errorHandler){
    console.log(personages);
    console.log(JSON.stringify(personages));
    var personagesObject = {personageLijst : JSON.stringify(personages)};
    if(personages !=null){
      $.ajax({
        type: 'post',
        url : target + '/characters/SaveAll',
        data: personagesObject,
        success: function(data){
          if (data.success){
            alert("characters uploaded to mongo");
            return true;
          }else{
            errorHandler(data.response);
          }
        },
        error : function(jqXHR, textStatus, errorThrown) {
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
        }
          
        }
      );
      console.log('should be updated');
    }};

    function deleteCharacters(personages,callback,errorHandler){
      $.ajax({
        type : 'DELETE',
        url : target + '/characters/KillAll',
        success : function(response){
          if (response == 'error'){
            console.log('Error in Deleting characters in mongo');
          }else{
            console.log('success in Deleting all characters in mongo');
            console.log(" Delete successful");
          }
        },
        error : function(jqXHR, textStatus, errorThrown) {
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
        }
      });
      alert('Delete command should be send');
    };

    



return {
  deleteCharacters : deleteCharacters,
  setCharacters : setCharacters,
  getCharacters : getCharacters
};





}();

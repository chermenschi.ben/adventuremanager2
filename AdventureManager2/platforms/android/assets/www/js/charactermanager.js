var Charactermanager = function(){

  //declaratie vars
  var _personageJson = {name : "testingson",
                        highconcept : "He is helpfull",
                        trouble : "he knows he will not be in the final app",
                        careful : 3,
                        clever : 2,
                        flashy : 2,
                        forceful : 0,
                        quick : 1,
                        sneaky :1,
                        description : "he aint much but he helps out once in a while"};
  var _personages = [_personageJson];
  var _currentPersonage;


//opstart van de functie
  var init = function(){
    _personages = null;
    console.log('awakening the character manager');
     var personages_str = localStorage.getItem('personages');
     if (personages_str !== null){
       _personages = JSON.parse(personages_str);
     }else{
       _personages = [_personageJson];
     }
     constructPersonages();
  };

//setting localStorage
  var _setLocalStorage = function(){
    console.log('setting local data ');
    console.log('setting personages');
    localStorage.setItem('personages', JSON.stringify(_personages));
    console.log(_personages);
    console.log('setting testingson');
    localStorage.setItem('testingson', JSON.stringify(_personageJson));
    init(); //grabbing the latest local storage data
   // constructPersonages();
  }

//personages
  //personages laden vanuit local storage.
  var constructPersonages = function() {
    console.log('constructing character list');

    console.log('emptying the ul');
    $('#ManageCharacters ul').empty();

    console.log('building all characters');

    console.log('length of _personages' + _personages.length);

    console.log('personages contains : ');
    console.log(_personages);

    console.log('Begin readout loop');
    for(var i=0; i< _personages.length; i++){

      console.log('+----------------------');
      console.log('|>loop turn : ' + i);
      console.log('|->character name : ' + _personages[i].name);

      var item = '<li class="collection-item">' +
              '<div class="title">'+ _personages[i].name +'</div>' +
              '<a class="btn-floating btn-large blue"   ><i class="editPersonage material-icons" data-Personage="' + i +'">create</i></a>' +
              '<a class=" btn-floating btn-large red" ><i class="deletePersonage material-icons" data-Personage="'+ i +'">delete_forever</i></a>' +
      '</li>';

      console.log('|->showing Item ');
      console.log('|===Begin of Item=======');
      console.log(item);
      console.log('|===End of Item=========');

      console.log('|->appending item');
      $('#ManageCharacters ul.collection').append(item);
      console.log('|->Append Successful');

      console.log('+-----------------------');
    };

    console.log('End of readout loop');


  };

  //personage toevoegen
  var addPersonage = function(){
    console.log('personage toevoegen');
    var naam = "personage" + (_personages.length +1);
    var personage = {name : naam,
                          highconcept : "",
                          trouble : "",
                          careful : 0,
                          clever : 0,
                          flashy : 0,
                          forceful : 0,
                          quick : 0,
                          sneaky :0,
                          description : ""};
  _personages.push(personage);
  _setLocalStorage();
};

//personage aanpassen
var editPersonage = function(id){
  console.log('charactermanager editing');
  console.log('gimme a sec to get yer data');
  console.log ('yer number is ' + id + ' you say? well lemme see ...');
  
  
  console.log('iz this you ?');
  console.log(_personages[id]);
  
    //data grab test 
    console.log('yer name is');
    console.log(_personages[id].name);
    console.log(_personages[id].careful);
  

    console.log("killing select");
    $('select').material_select('destroy');

    console.log("filling in the data");
  
  //invullen van veldgegevens
  
    $('#name').val(_personages[id].name);
    $('#description').val(_personages[id].description);
    $('#highconcept').val(_personages[id].highconcept);
    $('#trouble').val(_personages[id].trouble);
    $('#careful').val(_personages[id].careful);
    $('#clever').val(_personages[id].clever);
    $('#flashy').val(_personages[id].flashy);
    $('#forceful').val(_personages[id].forceful);
    $('#quick').val(_personages[id].quick);
    $('#sneaky').val(_personages[id].sneaky);
    $('#currentId').val(id);
//rebuilding select
$('select').material_select();
Materialize.updateTextFields();

  //laten zien van character editor
   $('.spa').hide();
    $('#CharacterEditor').show();
    $('.button-collapse').sideNav('hide');
  
};

 // personage opslaan
  var savePersonage = function(id){
    //quickcheck for id
    console.log('opslaan personage ');
    console.log(_personages[id]);

    //quickcheck for richt order
    if($('#currentId').val() !=""){

    
    console.log('testing the waters');
    console.log('naam');
    var name =  $('#name').val();
    console.log(name);
    var description = $('#description').val();
    var highconcept = $('#highconcept').val();
    var trouble = $('#trouble').val();
    var careful = $('#careful').val();
    var clever = $('#clever').val();
    var flashy = $('#flashy').val();
    var forceful = $('#forceful').val();
    var quick = $('#quick').val();
    var sneaky = $('#sneaky').val();

    //checking name
    if(name != "" ){
      _personages[id].name = name;
      _personages[id].description = description;
      _personages[id].highconcept = highconcept;
      _personages[id].trouble = trouble;
      _personages[id].careful = careful;
      _personages[id].clever = clever;
      _personages[id].flashy = flashy;
      _personages[id].forceful = forceful;
      _personages[id].quick = quick;
      _personages[id].sneaky = sneaky;
      console.log(_personages[id]);

      //setting data 
      _setLocalStorage();

      //laten zien van personage lijst
    $('.spa').hide();
    $('#ManageCharacters').show();
    $('.button-collapse').sideNav('hide');
    $('#currentId').val("");

  } else{
    //if no name given
      alert("Please name your character");
    }
} else{
  alert("please select a character to edit by pressing the blue button of its box");
  $('.spa').hide();
  $('#ManageCharacters').show();
  $('.button-collapse').sideNav('hide');
}
    //end of function
  };

   

  //personage verwijderen
  var deletePersonage = function(id){
    console.log('sending ' + _personages[id].name + ' to the execution room...');
    if(confirm('Dit personage permanent verwijderen?')){
      console.log(_personages[id].name + ' rests in peace');
      _personages.splice(id,1);
    }else{
      console.log(_personages[id].name + ' escaped');
    };
    _setLocalStorage();

  };


//server interaction
  //get local storage as json
  var getLocalStorageJson = function(){
    console.log("getting local storage Json");
    var personages_str = localStorage.getItem('personages');
     if (personages_str !== null){
       _personages = JSON.parse(personages_str);
     }
     
     return _personages;
  };
  
  //get local storage Stringified
  var getLocalStorageStringified = function(){
    console.log("getting local storage stringified");
    var personages_str = localStorage.getItem('personages');
     if (personages_str !== null){
       console.log(personages_str);
       return personages_str;
     }else{
       return False;
     }
  }





  return {
    init : init,
    constructPersonages : constructPersonages,
    addPersonage : addPersonage,
    editPersonage : editPersonage,
    savePersonage : savePersonage,
    deletePersonage : deletePersonage,
    getLocalStorageJson : getLocalStorageJson,
    getLocalStorageStringified : getLocalStorageStringified
  };
}()

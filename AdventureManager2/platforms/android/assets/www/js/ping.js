var serverPing = function(){
  var _adress="";
  var _Results ="";
  var pingDone = false;
  //var p, pingObject;
  var ipList = [{query: _adress, timeout: 1,retry: 3,version:'v4'}];
  var success = function (results) {
  /*  console.log('results : ');
    console.log(results);
    console.log(results[0].response.result);
    console.log(results[0].response.status); */
    var calculatedReceived = ((results[0].response.result.pctReceived / results[0].response.result.pctTransmitted)*100);
   /* console.log("calculated received");
    console.log(calculatedReceived);*/
    $('#ServerPing').empty();
    console.log('clearing ping data from div');

    var color = "red";
    if (results[0].response.status == "success"){
      color = "green";
    } 
    var pingdata = '<ul>' +
                    '<li>status : <a class="' + color +  ' btn">' + results[0].response.status + '</a></li>' +
                    '<li> target : ' + results[0].response.result.target + '</li>' +
                    '<li> packet send : ' + results[0].response.result.pctTransmitted  + '</li>' +
                    '<li> packets received : ' +   results[0].response.result.pctReceived+ ' <progress value ="' + results[0].response.result.pctReceived+'" max="' + results[0].response.result.pctTransmitted  + '"></progress>'+ calculatedReceived +'%</li>' +
                    '<li> packet loss : ' + results[0].response.result.pctLoss +'</li>' +
                    '</ul>';
    $('#ServerPing').append(pingdata);
    $('.spa').hide();
    $('#Ping').show();
    return results;
  };
  var err = function (e) {
    console.log('Error: ' + e);

     $('#ServerPing').empty();
    console.log('logging the error');
    var errordata = '<p> Error encountered : ' + e + '</p>';
                    
    $('#ServerPing').append(errordata);
    
  };

  var ping = function(adress){
    var p = new Ping();
   // console.log(adress);
    if(adress== null){
      _adress = "8.8.8.8";
    }else{
      _adress = adress;
    }
    ipList = [{query: _adress, timeout: 1,retry: 3,version:'v4'}];
    p.ping(ipList, success, err);
  }

  return {
    ping : ping
  }


}()
